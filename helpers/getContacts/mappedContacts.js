import getContacts from './getContacts';

const mapContacts = async () => {
  const avatarDefault = "https://portal.staralliance.com/imagelibrary/aux-pictures/prototype-images/avatar-default.png/@@images/image.png";
  const realContacts = await getContacts();
  
  const mapedContacts = realContacts.map(contact => {
    contactMapped = {
      id: contact.recordID,
      name: contact.displayName || null,
      phone: contact.phoneNumbers[0]?.number || null,
      avatar: contact.hasThumbnail ? contact.thumbnailPath : avatarDefault,
    };
    return contactMapped;
  })

  return mapedContacts;
}

export default mapContacts;