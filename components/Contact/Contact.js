import React from 'react';
import {StyleSheet, Text, View, Image} from 'react-native';

const Contact = ({ contact }) => {
  const {name, phone, avatar} = contact;

  return (
    <View style={styles.contact}>
      <Image
        style={styles.avatar}
        source={{
          uri: `${avatar}`,
        }}
      />
      <View style={styles.contactInfo}>
        <Text numberOfLines={2} style={styles.name}>{name}</Text>
        <Text style={styles.phone}>{phone}</Text>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  contact: {
    alignItems: 'center',
    padding: 10,
    marginBottom: 10,
    backgroundColor: 'white',
    borderRadius: 10,
    flexDirection: 'row',
  },
  avatar: {
    width: 60,
    height: 60,
    borderRadius: 30,
    marginRight: 20,
  },
  name: {
    fontSize: 16,
    fontWeight: '700',
    width: 200
  },
  phone: {
    color: 'grey',
    fontSize: 18
  },
});

export default Contact;