import {PermissionsAndroid} from 'react-native';
import Contacts from 'react-native-contacts';
import getContacts from '../getContacts/getContacts';

const delContact = async (id) => {
  
  const andoidContactPermission = PermissionsAndroid.request(
    PermissionsAndroid.PERMISSIONS.WRITE_CONTACTS,
    {
      title: "Contacts Permission",
      message:
        "This app would like to update your contacts.",
      buttonNeutral: "Ask Me Later",
      buttonNegative: "Cancel",
      buttonPositive: "OK"
    }
  );
  
  const realContacts = await getContacts();
  const contact = realContacts.find(contact => contact.recordID === id);

  Contacts.deleteContact(contact);
}

export default delContact;