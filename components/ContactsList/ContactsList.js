import React, {useState, useEffect} from 'react';
import {
  StyleSheet,
  SafeAreaView,
  View,
  TextInput,
  FlatList,
  TouchableOpacity,
  Button,
} from 'react-native';
import Contact from '../Contact/Contact';
import mapContacts from '../../helpers/getContacts/mappedContacts';
import delContact from '../../helpers/deleteContact/deleteContact';
import addContact from '../../helpers/addContact/addContact';

const ContactsList = ({route, navigation}) => {

  const [contactsData, setContactsData] = useState([]);
  const [contacts, setContacts] = useState(contactsData);
  const [inputText, setInputText] = useState('');

  useEffect(() => {
    mapContacts().then((data) => {
      setContactsData(data);
      setContacts(data);
    });
  }, []);

  const addHandler = () => {
    addContact().then(() => {
      mapContacts().then((data) => {
        setContactsData(data);
        setContacts(data);
      });
    })
  };

  const filterContacts = (text) => {
    setInputText(text);

    const filteredContacts = contactsData.filter(contact => {
      const contactName = contact.name ? contact.name.toLowerCase(): '';
      const filterName = text.toLowerCase();
      return contactName.includes(filterName);
    });

    setContacts(filteredContacts);
  };

  const deleteId = route.params?.delContact;

  useEffect(() => {

    if (deleteId) { 
      
      delContact(deleteId);
      mapContacts().then((data) => {
        setContactsData(data);
        setContacts(data);
      });
    }
  }, [deleteId]);

  const renderContact = ({item}) => (
    <TouchableOpacity
      onPress={() => navigation.navigate('Contact Info', item)}>
      <Contact contact={item} />
    </TouchableOpacity>
  );

  return (
    <SafeAreaView >
      <View style={styles.searchContainer}>
        <TextInput
          value={inputText}
          onChangeText={text => filterContacts(text)}
          style={styles.search}
        />
      </View>

      <View style={styles.btnAdd}>
        <Button title="Add contact" onPress={() => addHandler()} />
      </View>
      
      <FlatList
        data={contacts}
        renderItem={renderContact}
        keyExtractor={item => item.id}
        style={styles.contactList}
      />
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  searchContainer: {
    height: 60,
    backgroundColor: 'grey',
    justifyContent: 'center',
    alignItems: 'center',
  },
  search: {
    height: 40,
    width: '80%',
    backgroundColor: 'white',
    borderRadius: 5,
    fontSize: 18,
  },
  btnAdd: {
    alignSelf: 'center',
    width: '70%',
    margin: 10,
  },
  contactList: {
    paddingHorizontal: 10,
    paddingVertical: 10,
  },
});

export default ContactsList;
