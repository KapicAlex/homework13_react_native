import React from 'react';
import {
  StyleSheet,
  View,
  Text,
  Image,
  Button,
} from 'react-native';
import callPhone from '../../helpers/callPhone/callPhone'


const ContactDetails = ({route, navigation}) => {

  const {id, name, phone, avatar} = route.params;

  const deleteHandler = (id) => {
      navigation.navigate({
        name: 'Contacts List',
        params: {delContact: id},
    })};

  return (
    <View style={styles.container}>
      <Image
        style={styles.avatar}
        source={{
          uri: `${avatar}`,
        }}
      />
      <Text style={styles.name}>{name}</Text>
      <View style={styles.telContainer}>
          <Text style={styles.title}>Tel:</Text>
          <Text style={styles.phoneNumber}>{phone}</Text>
      </View>
      <View style={styles.buttonsContainer}>
        <Button
          color="red"
          title="Delete"
          onPress={() => deleteHandler(id)}
        />
        <Button title="Call" onPress={() => callPhone(phone)} />
      </View>
    </View>
  );
}


const styles = StyleSheet.create({
  container: {
    paddingTop: 30,
    alignItems: 'center',
  },
  avatar: {
    height: 200,
    width: 200,
    borderRadius: 100,
  },
  name: {
    fontSize: 26,
    fontWeight: '700',
    marginTop: 20,
    marginBottom: 20,
  },
  telContainer: {
    width: '90%',
    backgroundColor: 'white',
    borderRadius: 10,
    paddingVertical: 10,
    paddingHorizontal: 10,
    marginBottom: 30,
  },
  title: {
    fontWeight: '700',
    fontSize: 20,
  },
  phoneNumber: {
    fontSize: 18,
  },
  buttonsContainer: {
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
});

export default ContactDetails;
