import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import ContactsList from './components/ContactsList/ContactsList';
import ContactInfo from './components/ContactInfo/ContactInfo';

const Stack = createStackNavigator();

const App = () => {
  
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="Contacts">
        <Stack.Screen
          name="Contacts List"
          component={ContactsList}
        />
        <Stack.Screen name="Contact Info" component={ContactInfo} />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default App;
