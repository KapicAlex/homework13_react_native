import {PermissionsAndroid} from 'react-native';
import Contacts from 'react-native-contacts';
import getContacts from '../getContacts/getContacts';

const addContact = () => {
  
  const andoidContactPermission = PermissionsAndroid.request(
    PermissionsAndroid.PERMISSIONS.WRITE_CONTACTS,
    {
      title: "Contacts Permission",
      message:
        "This app would like to update your contacts.",
      buttonNeutral: "Ask Me Later",
      buttonNegative: "Cancel",
      buttonPositive: "OK"
    }
  );

  const newPerson = {
    displayName: ""
  }
  
  return Contacts.openContactForm(newPerson);

}

export default addContact;