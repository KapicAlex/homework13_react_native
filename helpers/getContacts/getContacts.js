import {PermissionsAndroid} from 'react-native';
import Contacts from 'react-native-contacts';

const getContacts = () => {
  
  const andoidContactPermission = PermissionsAndroid.request(
    PermissionsAndroid.PERMISSIONS.READ_CONTACTS,
    {
      title: "Contacts Permission",
      message:
        "This app would like to view your contacts.",
      buttonNeutral: "Ask Me Later",
      buttonNegative: "Cancel",
      buttonPositive: "OK"
    }
  );

  return Contacts.getAll()

}

export default getContacts;